-- [SECTION] Creation of Records / Insert
-- Syntax:
	-- INSERT INTO table_name (columns_in table) VALUES (values_per_column);

-- Insert One
INSERT INTO artists (name) VALUES ("Rivermaya");
INSERT INTO artists (name) VALUES ("Psy");
-- Insert Many
INSERT INTO artists (name) VALUES ("Taylor Swift"), ("Parokya ni Edgar");

-- Inserting into Albums table
INSERT INTO albums (Album_title, date_released, artist_id) VALUES ("Trip", "1996-1-1", 1);

INSERT INTO albums (Album_title, date_released, artist_id) VALUES ("Midnight (Taylor's Version)", "2023-1-1", 3), ("1989 (Taylor's Version)", "2023-1-1", 3), ("Bigotilyo", "2003-1-1", 4);

-- [SECTION] Read/Retrieving Records from db
-- Syntax:
	-- SELECT * FROM name_table;

-- Artist Table
SELECT * FROM artists;

-- Albums Table
SELECT * FROM albums;

-- Select the specific column from the table


-- We are going to add record in the songs table
-- Insert One
INSERT INTO songs (song_name, length, album_id, genre) VALUES ("Kundiman", 234, 1, "OPM");
-- Insert Many
INSERT INTO songs (song_name, length, album_id, genre) VALUES ("Alumni Homecoming", 442, 4, "OPM"), ("Snow on the Beach", 416, 2, "Pop"), ("Anti-Hero", 321, 2, "Pop");

-- Songs Table
SELECT * FROM songs;

-- Select particular column you want to view
SELECT song_name, length, genre FROM songs;

-- Retrieve OPM songs only
SELECT * FROM songs WHERE genre = "OPM";

-- Retrieve songs from a particular album
SELECT * FROM songs WHERE album_id = 2;

-- We can use AND and OR operator for multiple expressions
SELECT * FROM songs WHERE genre = "OPM" AND length > 234;
-- Alumni Homecoming

-- Using AND operator
SELECT song_name, length, genre FROM songs WHERE genre = "OPM" AND length > 234;
-- Using OR operator
SELECT song_name, length, genre FROM songs WHERE genre = "OPM" OR length > 234; 

-- [SECTION] Updating record
-- Syntax:
	-- UPDATE table_name SET column = value_to_be WHERE condition;

UPDATE songs SET length = 240 WHERE song_name = "Kundiman";

UPDATE songs SET song_name = "Trial" WHERE length > 234;

UPDATE songs SET id = 10 WHERE song_name = "Kundiman";

-- [SECTION] DELETING RECORD
-- Syntax:
	-- DELETE FROM table_name WHERE condition;
DELETE FROM songs WHERE length = 240;

-- Kung gusto mo na i-force yung id number sa data or table
INSERT INTO songs (id, song_name, length, album_id, genre) VALUES (5, "Trial Song", 234, 1, "OPM");