-- a.
SELECT * FROM artists WHERE name LIKE "%d%";

-- b. 
SELECT * FROM songs WHERE length < 350;

-- c.
SELECT albums.album_title, songs.song_name, songs.length FROM albums 
	JOIN songs 
	ON albums.id = songs.album_id;

-- d.
SELECT * FROM artists 
	JOIN albums
	ON artists.id = albums.artist_id
	WHERE album_title LIKE "%a%";

-- e.
SELECT * FROM albums WHERE id IN (1,2,3,4) ORDER BY album_title DESC;

-- f.
SELECT * FROM albums 
	JOIN songs 
	ON albums.id = songs.album_id
	ORDER BY album_title DESC;