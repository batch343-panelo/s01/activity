-- [SECTION] Advance Selects

-- Exclude records 
SELECT * FROM songs WHERE song_name != "Kundiman";

-- Exclude OPM songs and we want to show the song_name and length column only
SELECT song_name, length FROM songs WHERE genre != "opm";
SELECT song_name, length FROM songs WHERE genre != "OPM";
-- walang difference kung lowercase or uppercase

-- Greater than or equal operation
SELECT * FROM songs WHERE length > 300;
-- 300 is 3:00 or 3 minutes

-- Less than or equal operation
SELECT * FROM songs WHERE length < 300;

-- Songs that are longer than 3 minutes but shorter than 4 minutes
SELECT * FROM songs WHERE length > 300 AND length < 400;

-- Songs that are longer than or equal to 3 minutes but shorter than or equal to 4 minutes and 16 seconds
SELECT * FROM songs WHERE length >= 300 AND length <= 416;


-- OR operator
SELECT * FROM songs WHERE id = 1 OR id = 5 OR id = 3;

-- IN Operator
-- integer
SELECT * FROM songs WHERE id IN (1,3,5);
-- strings
SELECT * FROM songs WHERE genre IN ("Pop", "Pop rock");


-- Find partial matches

-- ang hinahanap mo is may y sa dulo
SELECT * FROM songs WHERE song_name LIKE "%y";

-- ang hinahanap mo is may Story sa dulo
SELECT * FROM songs WHERE song_name LIKE "%Story";

-- ang hinahanap mo is may k sa harap
SELECT * FROM songs WHERE song_name LIKE "k%";

-- ang hinahanap mo is may s sa harap and e sa dulo
SELECT * FROM songs WHERE song_name LIKE "s%e";

-- ang hinahanap mo is may r sa name
SELECT * FROM songs WHERE song_name LIKE "%r%";

-- ang hinahanap mo is nauuna si letter s then o but not necessarily magkasunod
SELECT * FROM songs WHERE song_name LIKE "%s%o%";

-- ang hinahanap mo is nauuna si letter l sa harap then r but not necessarily magkasunod
SELECT * FROM songs WHERE song_name LIKE "l%r%";


SELECT * FROM songs WHERE song_name LIKE "l%r%" AND genre = "Pop";

-- what if regardless of the sequence sir basta may s and o
SELECT * FROM songs WHERE song_name LIKE "%s%o%" OR song_name LIKE "%o%s%";


-- LIKE (INT) (Pattern):
-- kapag underscore dapat sakto yung characters
SELECT * FROM albums WHERE album_title LIKE "Tr_p";
-- kapag percent sign mas flexible kung mag ssearch ka
SELECT * FROM albums WHERE album_title LIKE "Tr%";

-- for id search
SELECT * FROM songs WHERE id LIKE "1_";
-- single digit
SELECT * FROM songs WHERE id LIKE "_";
-- double digit
SELECT * FROM songs WHERE id LIKE "__";

-- Sorting records
-- Ascending
SELECT * FROM songs ORDER BY song_name ASC;
-- Descending
SELECT * FROM songs ORDER BY song_name DESC;

-- Combination
SELECT * FROM songs WHERE song_name LIKE "%s%o%" ORDER BY song_name ASC;

-- GET distinct records
-- mga ibat ibang klase ng genre makikita mo
SELECT DISTINCT genre FROM songs;


-- [SECTION]: Table Joins
-- For Reference:
-- artists table: (id | name)
-- albums table: (id | album_title | date_released | artist_id)
-- songs table: (id | song_name | length | album_id | genre)

-- Join artists table and albums table
-- what the table looks like: (id | name | id | album_title | date_released | artist_id)
SELECT * 
	FROM artists 
	JOIN albums ON artists.id = albums.artist_id;

-- Join artists table and albums but with selected columns
-- what the table looks like: (name | album_title | date_released)
SELECT artists.name, albums.album_title, albums.date_released
	FROM artists 
	JOIN albums ON artists.id = albums.artist_id;

-- Join 3 tables artists, albums, songs
-- what the table looks like: (id | name | id | album_title | date_released | artist_id | id | song_name | length | album_id | genre)
SELECT * 
	FROM artists 
	JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;

-- Join 3 tables with selected columns
-- what the table looks like: (name | album_title | song_name)
SELECT artists.name, albums.album_title, songs.song_name 
	FROM artists 
	JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;

-- Join 3 tables with selected columns and results arranged in ascending order
-- what the table looks like: (name | album_title | song_name) contents sorted ascendingly (A-Z)
SELECT artists.name, albums.album_title, songs.song_name 
	FROM artists 
	JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id
	ORDER BY song_name ASC;

-- Join 3 tables with selected columns where songs starts with letter L and arranged in ascending order
-- what the table looks like: (name | album_title | song_name) table contains songs that start with letter L and are arranged in ascending order
SELECT artists.name, albums.album_title, songs.song_name 
	FROM artists 
	JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id
	WHERE song_name LIKE "l%"
	ORDER BY song_name ASC;